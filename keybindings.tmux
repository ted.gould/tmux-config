unbind-key -n C-a
set -g prefix ^A
set -g prefix2 ^A
bind a send-prefix

# Vim movements between panes
bind-key -T prefix j select-pane -U
bind-key -T prefix k select-pane -D
bind-key -T prefix h select-pane -L
bind-key -T prefix l select-pane -R

# Split and join panes
bind < split-window -h \; choose-window 'kill-pane ; join-pane -hs %%'
bind > break-pane

